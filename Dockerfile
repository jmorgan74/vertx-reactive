FROM java:8
EXPOSE 5640:5640
RUN mkdir /opt/java
COPY build/libs/vertx-reactive-1.0-SNAPSHOT-fat.jar /opt/java
WORKDIR /opt/java
ENTRYPOINT java -jar vertx-reactive-1.0-SNAPSHOT-fat.jar

