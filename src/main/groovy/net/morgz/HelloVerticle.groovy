package net.morgz

import groovy.util.logging.Slf4j
import io.vertx.lang.groovy.GroovyVerticle

@Slf4j
class HelloVerticle extends GroovyVerticle {

    def totalRequests = 0
    def prevRequests = 0

    void start() {

        def server = vertx.createHttpServer()

        server.requestHandler({ request ->

            this.totalRequests++

            // This handler gets called for each request that arrives on the server
            def response = request.response()

            response.putHeader("content-type", "text/plain")

            // Write to the response and end it
            response.end("Vert.x Reactive - Hello World!")
        })

        vertx.setPeriodic(1000, { id ->
            log.info("Rx/s: ${this.totalRequests - this.prevRequests}")

            this.prevRequests = this.totalRequests
        })

        server.listen(5640)

    }

}